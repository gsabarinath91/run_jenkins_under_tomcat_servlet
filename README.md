# README #

This repository conatins two files,
1.server.xml     (tomcat server.xml) 
2.tomcat.service (systemd unit file)

These files used to run jenkins on tomcat servlet and access.

server.xml - contains setting of custom port changed from 8080 to 9080,Also indeed context path settings to avoing /jenkins in the URL.
tomcat.service - systemd unit file to covert as systemd Daemon

### What is this repository for? ###

* Run jenkins on tomcat servlet

### How do I get set up? ###

* Place server.xml under your $CATALINA_HOME/conf/
* Place tomcat.service under /etc/systemd/system/


### Who do I talk to? ###

* g.sabarinath91@gmail.com